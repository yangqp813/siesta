add_executable( grid1d grid1d.f )
add_executable( grid2d grid2d.f )

target_link_libraries( grid1d
  PRIVATE
  ${PROJECT_NAME}-libunits
  )
target_link_libraries( grid2d
  PRIVATE
  ${PROJECT_NAME}-libunits
  )

if( SIESTA_INSTALL )
  install(
    TARGETS grid1d grid2d
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
    )
endif()
