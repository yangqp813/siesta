#
# FIXME: The names in this directory are very confusing
#        Only a subset of the executables are built
#
set(top_srcdir "${PROJECT_SOURCE_DIR}/Src")


#
# Create auxiliary libraries, with their own module directories,
# to satisfy Ninja. This is also good practice in general
# 
# -- Sockets
siesta_add_library(sockets-siesta-dispatch 
    ${top_srcdir}/fsiesta_sockets.F90
    ${top_srcdir}/fsockets.f90
    ${top_srcdir}/sockets.c
    ${top_srcdir}/posix_calls.f90
)
set_target_properties(sockets-siesta-dispatch
  PROPERTIES
  Fortran_MODULE_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/sockets-include")

target_include_directories(sockets-siesta-dispatch
  INTERFACE
  "${CMAKE_CURRENT_BINARY_DIR}/sockets-include")

# -- Pipes
siesta_add_library(pipes-siesta-dispatch 
    ${top_srcdir}/fsiesta_pipes.F90
    ${top_srcdir}/pxf.F90
    ${top_srcdir}/posix_calls.f90
)
set_target_properties(pipes-siesta-dispatch
  PROPERTIES
  Fortran_MODULE_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/pipes-include")
target_include_directories(pipes-siesta-dispatch
  INTERFACE
  "${CMAKE_CURRENT_BINARY_DIR}/pipes-include")

if (SIESTA_WITH_MPI)

# -- MPI dispatch -- uses 'libsiesta'
siesta_add_library(mpi-siesta-dispatch
  ${top_srcdir}/fsiesta_mpi.F90
)
target_link_libraries(mpi-siesta-dispatch PUBLIC SIESTA_libsiesta)

set_target_properties(mpi-siesta-dispatch
  PROPERTIES
  Fortran_MODULE_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/mpi-include")
target_include_directories(mpi-siesta-dispatch
  INTERFACE
  "${CMAKE_CURRENT_BINARY_DIR}/mpi-include")

# ---- 
#
# Phonons: Uses the MPI dispatch
#
siesta_add_executable(phonons
  phonons.f90)
target_link_libraries(phonons PRIVATE mpi-siesta-dispatch)


# Simple MPI-aware driver
siesta_add_executable(mpi_driver
    simple_mpi_parallel.f90
)
target_link_libraries(mpi_driver PRIVATE mpi-siesta-dispatch)


# Sockets interface to parallel-version of Siesta
siesta_add_executable(sockets_parallel  simple_parallel.f90)
target_link_libraries(sockets_parallel PRIVATE sockets-siesta-dispatch)


# Pipes interface to parallel-version of Siesta
siesta_add_executable(pipes_parallel
    simple_parallel.f90
)
target_link_libraries(pipes_parallel PRIVATE pipes-siesta-dispatch)


if( SIESTA_INSTALL )
  install(
    TARGETS
      phonons pipes_parallel
      sockets_parallel mpi_driver
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
    )
endif()

else(SIESTA_WITH_MPI)

siesta_add_executable(sockets_serial  simple_serial.f90)
target_link_libraries(sockets_serial PRIVATE sockets-siesta-dispatch)

siesta_add_executable(pipes_serial simple_serial.f90)
target_link_libraries(pipes_serial PRIVATE pipes-siesta-dispatch)

if( SIESTA_INSTALL )
  install(
    TARGETS
      pipes_serial
      sockets_serial
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
    )
endif()

endif(SIESTA_WITH_MPI)




  
