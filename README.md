<div align="center">

![SIESTA logo](https://siesta-project.org/siesta/SIESTA-logo-233x125.png)
</div>

------------------------
<div align="center">

**S**panish **I**nitiative for **E**lectronic **S**imulations with **T**housands of **A**toms
</div>

[![Original paper](https://img.shields.io/endpoint?url=https%3A%2F%2Fapi.juleskreuer.eu%2Fcitation-badge.php%3Fshield%26doi%3D10.1088%2F0953-8984%2F14%2F11%2F302&label=Original%20paper)](https://doi.org/10.1088/0953-8984/14/11/302)
[![Latest paper](https://img.shields.io/endpoint?url=https%3A%2F%2Fapi.juleskreuer.eu%2Fcitation-badge.php%3Fshield%26doi%3D10.1063%2F5.0005077&label=Latest%20paper)](https://doi.org/10.1063/5.0005077)
[![Original TranSIESTA paper](https://img.shields.io/endpoint?url=https%3A%2F%2Fapi.juleskreuer.eu%2Fcitation-badge.php%3Fshield%26doi%3D10.1103%2FPhysRevB.65.165401&label=Original%20TranSIESTA%20paper)](https://doi.org/10.1103/PhysRevB.65.165401)
[![Latest TranSIESTA paper](https://img.shields.io/endpoint?url=https%3A%2F%2Fapi.juleskreuer.eu%2Fcitation-badge.php%3Fshield%26doi%3D10.1016%2Fj.cpc.2016.09.022&label=Latest%20TranSIESTA%20paper)](https://doi.org/10.1016/j.cpc.2016.09.022)




[![GitLab Release](https://img.shields.io/gitlab/v/release/siesta-project%2Fsiesta?logo=gitlab)](https://gitlab.com/siesta-project/siesta/-/releases)
[![GitLab last commit](https://badgen.net/gitlab/last-commit/siesta-project/siesta/)](https://gitlab.com/siesta-project/siesta/-/commits)
[![License](https://badgen.net/gitlab/license/siesta-project/siesta?label=License)](https://gitlab.com/siesta-project/siesta/-/blob/master/COPYING)

[![Install SIESTA using conda](https://img.shields.io/conda/v/conda-forge/siesta)](https://anaconda.org/conda-forge/siesta)
[![Conda Downloads](https://anaconda.org/conda-forge/siesta/badges/downloads.svg)](https://anaconda.org/conda-forge/siesta)

[![Join discussion on Discord](https://img.shields.io/discord/821394364336046111.svg?label=&logo=discord&logoColor=ffffff&color=green&labelColor=red)](https://discord.gg/AqjX6aTNXR)
[![Ask on matter modeling](https://img.shields.io/badge/Ask_on_Stack_Exchange-whitesmoke.svg?logo=stackexchange)](https://mattermodeling.stackexchange.com/questions/tagged/siesta)


**SIESTA** is a program for **efficient electronic structure calculations**
and ab initio molecular dynamics simulations of molecules and
solids in the framework of **Density-Functional Theory (DFT)**. 

SIESTA's efficiency stems from the use of a basis set of
**strictly-localized atomic orbitals**. A very important feature of the
code is that its accuracy and cost can be tuned in a wide range, from
quick exploratory calculations to highly accurate simulations **matching
the quality of other approaches, such as plane-wave methods**.

SIESTA's performance advantage enables the treatment of systems with **hundreds of
atoms in modest hardware**, scaling up to **tens of thousands**
and more when deployed **in supercomputers**.

- **Web page**: https://siesta-project.org.
- **Documentation**: https://docs.siesta-project.org/projects/siesta
- **Source code**: https://gitlab.com/siesta-project/siesta
- **Installation** : https://docs.siesta-project.org/projects/siesta/en/latest/how-to/index.html#building-siesta
- **Installation** : https://docs.siesta-project.org/projects/siesta/en/latest/installation/quick-install.html
- **Tutorials**: https://docs.siesta-project.org/projects/siesta/en/latest/tutorials/index.html
- **PDF manuals**: https://siesta-project.org/SIESTA_MATERIAL/Docs/Manuals/manuals.html
- **Compatible pseudopotentials**: http://www.pseudo-dojo.org/
- **Dev and versions guide**: https://gitlab.com/siesta-project/siesta/-/wikis/Guide-to-Siesta-versions
- **Issues/bugs**: https://gitlab.com/siesta-project/siesta/-/issues (see [guidelines](https://gitlab.com/siesta-project/siesta/-/blob/master/Docs/REPORTING_BUGS?ref_type=heads))

**TranSiesta** is now part of the SIESTA executable, see the documentation for details.
